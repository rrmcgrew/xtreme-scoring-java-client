package com.finnimcfinger.xtremeapi.model;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class Rating {
    private final Driver driver;

    private final int rank;
    private final int poles;
    private final int top10;
    private final int top5;
    private final int wins;
    private final int events;

    private final double rating;
    private final double avgStart;
    private final double avgFinish;
    private final double incPerLap;
    private final double lapsPerInc;
}
