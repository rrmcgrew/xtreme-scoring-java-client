package com.finnimcfinger.xtremeapi.model;

import lombok.*;;

@Getter
@Builder(toBuilder = true)
@ToString
public class Organization {
    private final String name;
    private final String description;
    private final String status;
    private final String organizationId;
}
