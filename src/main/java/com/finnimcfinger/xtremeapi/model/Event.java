package com.finnimcfinger.xtremeapi.model;

import java.util.Date;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class Event {
    public final Track track;
    public final TrackConfiguration trackConfiguration;

    public final Date broadcastDate;
    public final Date eventDate;
    
    public final String broadcastLink;
    public final String broadcastText;
    public final String description;
    public final String eventLogoPath;
    public final String eventName;
    public final String qualSetupName;
    public final String raceSetupName;
    public final String organizationId;
    public final String seriesId;
    public final String eventId;
    public final String sponsor;
    public final String typeEvent;
    public final String raceSessionType;
    public final String raceLengthStr;

    public final boolean canDrop;
    public final boolean countAsKept;
    public final boolean isFixedSetup;
    public final boolean pointsCount;
    public final boolean raceResultsLoaded;

    private final int numberPlayoffDrivers;
    private final int playoffRound;
}
