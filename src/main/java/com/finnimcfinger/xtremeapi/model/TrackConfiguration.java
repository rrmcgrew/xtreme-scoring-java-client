package com.finnimcfinger.xtremeapi.model;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class TrackConfiguration {
    public final String configurationId;
    public final String length;
    public final String lengthKm;
    public final String pitRoadSpeedLimit;
    public final String pitRoadSpeedLimitKm;
    public final String name;
    public final String typeTrack;

    public final int maxCars;
    public final int numberPitStalls;
    public final int numTurns;
}
