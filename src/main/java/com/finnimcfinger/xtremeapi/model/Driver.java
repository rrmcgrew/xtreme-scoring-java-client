package com.finnimcfinger.xtremeapi.model;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class Driver {
    private final String iRacingId;
    private final String driverId;
    private final String organizationId;
    private final String name;
    private final String preferredName;
    private final String hometown;
    private final String countryCode;
    private final String country;
    private final String status;
}
