package com.finnimcfinger.xtremeapi.model;

import java.util.Date;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class Track {
    public final String trackId;
    public final String name;
    public final String packageId;
    public final String siteUrl;
    public final String coordinates;
    public final String country;
    public final String location;
    public final String logoImagePath;
    
    public final Date dateCloses;
    public final Date dateOpens;

    public final int gridStalls;
}
