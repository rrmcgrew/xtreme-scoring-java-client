package com.finnimcfinger.xtremeapi.model;

import java.util.Date;

import lombok.*;

@Getter
@Builder(toBuilder = true)
@ToString
public class Season {
    private final String seasonName;
    private final String description;
    private final String organizationId;
    private final String seriesId;
    private final String seasonId;
    
    private final Date startDate;

    private final boolean hasHeats;
    private final boolean hasPlayoff;
    private final boolean hideFromView;
    private final boolean isActive;
    private final boolean isMultiClass;

    private final int maxDrivers;
    private final int minDrivers;
    private final int numCompletedRaces;
    private final int numScheduledRaces;
}
