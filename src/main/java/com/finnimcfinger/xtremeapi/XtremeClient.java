package com.finnimcfinger.xtremeapi;

import java.util.List;

import com.finnimcfinger.xtremeapi.model.*;
import com.finnimcfinger.xtremeapi.util.*;
import com.google.gson.*;
import com.google.gson.reflect.*;

public class XtremeClient {
    private static final String BASE_URL = "https://api.xtremescoring.com";
    private static final String ORGS = "/organizations";
    private static final String RATINGS = "/ratings";
    private static final String SERIES = "/series";
    private static final String SEASONS = "/seasons";
    private static final String EVENTS = "/events";

    private HttpClientUtil clientUtil;
    private Gson gson;

    /**
     * Creates a new client object. An API key must be provided.
     * 
     * @param apiKey The alphanumeric key used to allow access to the API. Example
     *               "lgfo8za8dcdm8sc0aifkfi7z9haj4har"
     * @throws IllegalArgumentException Thrown when API key is null or empty.
     */
    public XtremeClient(String apiKey) {
        if (apiKey == null || apiKey.trim().isEmpty()) {
            throw new IllegalArgumentException("api key cannot be empty or null");
        }

        this.clientUtil = new HttpClientUtil(apiKey, BASE_URL);
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .create();
    }

    /**
     * Fetches the organizations to which the API user has access.
     * 
     * @return A list of organizations.
     */
    public List<Organization> fetchOrganizations() {
        String responseBody = clientUtil.doGetForJsonString(ORGS);
        List<Organization> orgs = getListByArrayNameForTypeFromJson("Organizations", Organization.class, responseBody);

        return orgs;
    }

    /**
     * Fetches the details for a specific organization, identified by its ID.
     * 
     * @param orgId The unique ID for the requested organization. Example
     *              "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return Object containing the details for the requested organization.
     */
    public Organization fetchOrganizationById(String orgId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId);
        Organization org = getObjectByNameForTypeFromJson("Organization", Organization.class, responseBody);

        return org;
    }

    /**
     * Fetches the driver ratings for an organization. This will only work for
     * organizations with multiple series.
     * 
     * @param orgId The unique identifier for the organization. Example
     *              "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return A list of ratings for the drivers in the organization.
     */
    public List<Rating> fetchOrgDriverRatings(String orgId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId, RATINGS);
        List<Rating> ratings = getListByArrayNameForTypeFromJson("Ratings", Rating.class, responseBody);

        return ratings;
    }

    /**
     * Fetches the series for a given organization.
     * 
     * @param orgId The unique identifier for the organization for which series are
     *              to be fetched. Example "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return A list of series for the given organization
     */
    public List<Series> fetchOrgSeries(String orgId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId, SERIES);
        List<Series> series = getListByArrayNameForTypeFromJson("Series", Series.class, responseBody);

        return series;
    }

    /**
     * Fetches the details for a specific series for a given organization.
     * 
     * @param orgId    The unique identifier for the organization to which the
     *                 series belongs. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @param seriesId The unigue identifier for the series. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return Object containing the details for the requested series.
     */
    public Series fetchSeries(String orgId, String seriesId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId, SERIES, seriesId);
        Series series = getObjectByNameForTypeFromJson("Series", Series.class, responseBody);

        return series;
    }

    /**
     * Fetches driver ratings for a series.
     * 
     * @param orgId    The unique identifier for the organization to which the
     *                 series belongs. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @param seriesId The unigue identifier for the series. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return A list of ratings for the drivers in the series.
     */
    public List<Rating> fetchSeriesDriverRatings(String orgId, String seriesId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId, SERIES, seriesId, RATINGS);
        List<Rating> ratings = getListByArrayNameForTypeFromJson("Ratings", Rating.class, responseBody);

        return ratings;
    }

    /**
     * Fetches the seasons for a given series.
     * 
     * @param orgId    The unique identifier for the organization to which the
     *                 series belongs. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @param seriesId The unigue identifier for the series. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return A list of seasons for the series.
     */
    public List<Season> fetchSeriesSeasons(String orgId, String seriesId) {
        String responseBody = clientUtil.doGetForJsonString(ORGS, orgId, SERIES, seriesId, SEASONS);
        List<Season> seasons = getListByArrayNameForTypeFromJson("Seasons", Season.class, responseBody);

        return seasons;
    }

    /**
     * Fetches the events for a given season.
     * 
     * @param seriesId The unigue identifier for the series. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @param seasonId The unigue identifier for the season. Example
     *                 "abcd1234-zyxa-1234-09ab-aaafff123456"
     * @return A list of events for the season.
     */
    public List<Event> fetchSeasonEvents(String seriesId, String seasonId) {
        String responseBody = clientUtil.doGetForJsonString(SERIES, seriesId, SEASONS, seasonId, EVENTS);
        List<Event> events = getListByArrayNameForTypeFromJson("Events", Event.class, responseBody);

        return events;
    }

    private <T> List<T> getListByArrayNameForTypeFromJson(String arrayName, Class<T> type, String jsonRaw) {
        JsonObject json = gson.fromJson(jsonRaw, JsonObject.class);
        TypeToken<?> typeToken = TypeToken.getParameterized(List.class, type);
        JsonArray objects = json.get(arrayName).getAsJsonArray();
        List<T> list = gson.fromJson(objects, typeToken.getType());

        return list;
    }

    private <T> T getObjectByNameForTypeFromJson(String objectName, Class<T> type, String jsonRaw) {
        JsonObject json = gson.fromJson(jsonRaw, JsonObject.class);
        JsonObject object = json.get(objectName).getAsJsonObject();
        T result = gson.fromJson(object, type);

        return result;
    }
}
