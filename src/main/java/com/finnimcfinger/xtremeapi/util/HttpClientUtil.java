package com.finnimcfinger.xtremeapi.util;

import javax.ws.rs.client.*;
import javax.ws.rs.core.*;

public class HttpClientUtil {
    private static final String API_KEY_HEADER = "x-api-key";

    private final String apiKey;
    private final String baseUrl;

    public HttpClientUtil(String apiKey, String baseUrl) {
        this.apiKey = apiKey;
        this.baseUrl = baseUrl;
    }

    public String doGetForJsonString(String... pathElements) {
        String path = buildPath(pathElements);
        
        return doGetForJsonString(path);
    }

    private String doGetForJsonString(String path) {
        Client client = ClientBuilder.newClient();

        return client
                .target(this.baseUrl)
                .path(path)
                .request(MediaType.APPLICATION_JSON)
                .header(API_KEY_HEADER, this.apiKey)
                .get(String.class);
    }

    public String buildPath(String... elements) {
        StringBuilder path = new StringBuilder(elements[0]);

        for (int i = 1; i < elements.length; i++) {
            if (!elements[i].startsWith("/")) {
                path.append("/");
            }

            path.append(elements[i]);
        }

        return path.toString();
    }
}
